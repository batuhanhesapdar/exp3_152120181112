/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Square.h"

Square::Square(double a) {
	setA(a);
	setB(a);
}

Square::~Square() {
}

void Square::setA(double a){
	a = a; 
}

void Square::setB(double b){
	b = b; 
}

double Square::calculateCircumference(){// this method changed from a + b * 2 to (a + b) * 2 to work correctly
	return (a + b) * 2;
}

double Square::calculateArea(){
	return a * b;
}
