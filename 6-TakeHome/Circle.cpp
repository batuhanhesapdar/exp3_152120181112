/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Circle.h"

Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(int r){
	this->r = r; 
}

void Circle::setR(double r){
	this->r = r; 
}

double Circle::getR() const{
	return r;
}

double Circle::calculateCircumference() const{ // this method changed from PI * r * r to PI * r * 2 to work correctly.
	return PI * r * 2;
}

double Circle::calculateArea(){// this method changed from PI * r * 2 to PI * r * r to work correctly.
	return PI * r * r;
}
bool Circle::isEqual(Circle otherCircle1,Circle otherCircle2){
	if(otherCircle1.getR()==otherCircle2.getR()){
		return true;
	}
	else
	{
		return false;
	}
}


