/*
 * Triangle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Triangle.h"

Triangle::Triangle(double a, double b, double c) {
	setA(a);
	setB(b);
	setC(c);
}

Triangle::~Triangle() {

}

void Triangle::setA(double a){
	a = a;
}

void Triangle::setB(double){
	b = b;
}

void Triangle::setC(double){ // this method changed from c=a to c=c to work correctly
	c = c;
}

double Triangle::calculateCircumference(){// this method changed from  a+ b + c/2  to a+ b + c to work correctly
	return a + b + c; 
}

